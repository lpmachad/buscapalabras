#!/usr/bin/env python3

"""
Busca una palabra en una lista de palabras
"""

import sortwords
import sys

# Función que busca una palabra en una lista y devuelve su índice si se encuentra.
def search_word(word, words_list):
    try:
        word_lower = word.lower()
        if word_lower in words_list:
            for w in range(len(words_list)):
                if word_lower == words_list[w].lower():
                    test_index = w
                    return test_index
    except ValueError:
        sys.exit()

# Función principal del script
def main():
    word = sys.argv[1]
    words_list = sys.argv[2:]

    # Verifica que se proporcionen al menos dos argumentos
    if len(words_list) < 2:
        print("Error: python3 searchwords.py <word to search> <at least two arguments must be provided>")
        sys.exit()
    else:
        # Ordena la lista de palabras utilizando la función sortwords.sort
        ordered_list = sortwords.sort(words_list)
        # Muestra la lista ordenada
        sortwords.show(ordered_list)

        # Busca la posición de la palabra en la lista ordenada
        position = search_word(word, ordered_list)
        print(position)

# Ejecuta el script si es el programa principal
if __name__ == '__main__':
    main()

